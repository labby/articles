{#
/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 #}
 
{% autoescape false %}
<div class="ui {{ oArt.addon_color }} segment">
	<div class="ui basic segment">
		<form class="ui form" action="{{ oArt.action_url }}" method="post">
			<input type="hidden" name="post_id" value="{{ current_article.post_id }}" />
			<input type="hidden" name="leptoken" value="{{ leptoken }}" />		
			<h3 class="ui header">{{ oArt.language.article }} {{ oArt.language.edit }}</h3>					
			<div class="field">
				<label>{{ oArt.language.post_title }}</label>
				<input name="post_title" type="text" value="{{ current_article.post_title }}">
			</div>					
			<div class="spacer2"></div>	
			<div class="three fields">
				<div class="field">
					<label>{{ oArt.language.group }}</label>
					<select name="group_id" class="ui fluid dropdown">
						{% for item in oArt.active_groups %}
							<option value ="{{ item.group_id }}" {% if current_article.group_id == item.group_id %} selected="selected"{% endif %}>{{ item.group_title }}</option>
						{% endfor %}							
					</select>
				</div>			
				<div class="field">
					<label>{{ oArt.language.post_link }}</label>
					<input name="post_link" type="text" value="{{ current_article.post_link }}">
				</div>
				<div class="field">
					<label>{{ oArt.language.post_url }}</label>
					<input name="post_url" type="text" value="{{ current_article.post_url }}">
				</div>				
			</div>				
	
			{% if !fileExists(oArt.image_path~'/'~ current_article.post_id~'.png') %}
				<div class="spacer2"></div>	
				<div class="ui icon message">
					<i class="image outline icon"></i>
					<div class="content">
						<div class="header">{{ oArt.language.teaser_image }}</div>
						<p>{{ oArt.language.teaser_image_text1 }} <b>{{ MEDIA_DIRECTORY }}/articles/</b> <br />{{ oArt.language.teaser_image_text2 }} <b>{{ current_article.post_id~'.png' }}</b></p>
					</div>
				</div>
			{% endif %}
			
			<div class="spacer2"></div>	
			<div class="field">
				<label>{{ oArt.language.teaser }}</label>
				<textarea id="no_wysiwyg" name="post_teaser" rows="5">{{ current_article.post_teaser }}</textarea>
			</div>			
			<div class="spacer2"></div>			
			<div class="field">
				<label>{{ oArt.language.content }}</label>
				{{ display_content }}		
			</div>			
			<div class="spacer2"></div>
			<div class="two fields">
				<div class="field">
					<label>{{ oArt.language.publish_start }}</label>
					<input name="publish_start" type="date" value="{{ current_article.publish_start }}">
				</div>
				<div class="field">
					<label>{{ oArt.language.publish_end }}</label>
					<input name="publish_end" type="date" value="{{ current_article.publish_end }}">
				</div>							
			</div>
			<div class="spacer2"></div>				
			<div class="field">
				<label>{{ oArt.language.post_tags }}</label>
				<input name="post_tags" type="text" value="{{ current_article.post_tags }}">
			</div>
			<div class="spacer2"></div>	
			<div class="four disabled fields">			
				<div class="field">
					<label>{{ oArt.language.posted_when }}</label>
					<input name="posted_when" type="text" value="{{ current_article.posted_when|date("d.m.Y") }}">
				</div>
				<div class="field">
					<label>{{ oArt.language.posted_by }}</label>
					<input name="display_name" type="text" value="{{ display_name }}">
					<input name="posted_by" type="hidden" value="{{ current_article.posted_by }}">
				</div>
				<div class="field">
					<label>{{ oArt.language.modified_when }}</label>
					<input name="modified_when" type="text" value="{{ current_article.modified_when|date("d.m.Y") }}">
				</div>
				<div class="field">
					<label>{{ oArt.language.modified_by }}</label>
					<input name="modified_name" type="text" value="{{ modified_name }}">
					<input name="modified_by" type="hidden" value="{{ current_article.modified_by }}">
				</div>				
			</div>				
			<div class="spacer2"></div>			
			
			<button class="ui positive button" name="save_article" value="{{ current_article.post_id }}" type="submit"><i class="save icon"></i>{{ TEXT.SAVE }}</button>
			<button class="ui {{ oArt.addon_color }} button" name="list_articles" value="list" type="submit"><i class="reply icon"></i>{{ TEXT.BACK }}</button>
			<div class="spacer2"></div>			
		</form>
		<div class="spacer2"></div>					
	</div>
</div>
{% endautoescape %}