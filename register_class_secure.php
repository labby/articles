<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */


$files_to_register = array(
    'atom.php',
    'rss.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
