<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php



$database = LEPTON_database::getInstance();

// import data from news tables
$import_news = $database->get_one("SELECT `import_news` FROM ".TABLE_PREFIX."mod_articles_settings WHERE id = 1 ");
if($import_news == 0)
{
	/**
	 * NOTICE: for details see readme file
	 * 
	 */	
	
	// get current output path
	$output_path = $database->get_one("SELECT `dir_name` FROM ".TABLE_PREFIX."mod_articles_settings WHERE id = 1 ");
	 

	 // [0]  insert no group if not there
	$check_group = $database->get_one("SELECT `group_id` FROM ".TABLE_PREFIX."mod_news_groups WHERE group_id = 0 ");
	if($check_group == NULL)
	{
		// insert no group
		$field_values="(NULL,0,0,1,0,'no group')";
		LEPTON_handle::insert_values("mod_news_groups",$field_values);
		
		// force group_id to 0
		$last_id = $database->get_one("SELECT LAST_INSERT_ID() FROM ".TABLE_PREFIX."mod_news_groups");
		$database->simple_query("UPDATE ".TABLE_PREFIX."mod_news_groups SET `group_id` = 0 WHERE `group_id` = ".$last_id);
	}

	 // [1] import news groups table 
	LEPTON_handle::create_sik_table('mod_articles_groups');
	$database->simple_query("TRUNCATE ".TABLE_PREFIX."mod_articles_groups ");	
	
	$news_groups = [];
	$database->execute_query(
		"SELECT * FROM ".TABLE_PREFIX."mod_news_groups ORDER BY group_id ASC",
		true,
		$news_groups,
		true
	);	

	if(!empty($news_groups))
	{
		
		foreach($news_groups as $article_group)
		{
			$_POST['group_title'] = $article_group['title'];
			$_POST['active'] = $article_group['active'];

			$request = LEPTON_request::getInstance();
		
			$all_names = array (
				'group_title'	=> array ('type' => 'string_clean', 'default' => ""),
				'active'		=> array ('type' => 'integer', 'default' => 1)
			);		

			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_articles_groups";
			$database->build_and_execute( 
				'INSERT', 
				$table, 
				$all_values
			);			
		}

		$database->simple_query("UPDATE ".$table." SET `group_id` = 0 WHERE `group_id` = 1 ");		
		echo (LEPTON_tools::display('Table <b>mod_news_groups</b> finished','pre','ui positive message')); 	
	}
	else
	{
		echo (LEPTON_tools::display('There are no groups in table <b>mod_news_groups</b>!','pre','ui positive message')); 
	}


	 // [2] import news items table 
	LEPTON_handle::create_sik_table('mod_articles');
	$database->simple_query("TRUNCATE ".TABLE_PREFIX."mod_articles ");
	 
	$news_items = [];
	$database->execute_query(
		"SELECT * FROM ".TABLE_PREFIX."mod_news_posts",
		true,
		$news_items,
		true
	);

	if(!empty($news_items))
	{
		foreach($news_items as $article)
		{
			// modify content
			$teaser_temp = htmlspecialchars_decode($article['content_short']);
			$content_temp = htmlspecialchars_decode($article['content_long']);
			$publish_start = gmdate("Y-m-d", $article['published_when']);
			if($article['published_until'] == 0)
			{
				$publish_end = '';
			}
			else
			{
				$publish_end = gmdate("Y-m-d", $article['published_until']);
			}
			
			$_POST['post_title'] = $article['title'];
			$_POST['post_link'] = $article['link'];
			$_POST['post_url'] = $article['commenting'];
			$_POST['post_teaser'] = strip_tags($teaser_temp);
			$_POST['post_content'] = $content_temp;	//$article['content_long'];
			$_POST['group_id'] = $article['group_id'];
			$_POST['publish_start'] = $publish_start;
			$_POST['publish_end'] = $publish_end;
			$_POST['posted_when'] = $article['posted_when'];
			$_POST['posted_by'] = $article['posted_by'];
			$_POST['active'] = $article['active'];

			$request = LEPTON_request::getInstance();
		
			$all_names = array (
				'post_title'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_link'		=> array ('type' => 'string_clean', 'default' => ""),
				'post_url'		=> array ('type' => 'string_clean', 'default' => ""),				
				'post_teaser'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_content'	=> array ('type' => 'string_chars', 'default' => ""),
				'group_id'		=> array ('type' => 'integer', 'default' => 0),
				'publish_start'	=> array ('type' => 'date', 'default' => NULL),
				'publish_end'	=> array ('type' => 'date', 'default' => NULL),
				'posted_when'	=> array ('type' => 'integer', 'default' => 0),
				'posted_by'		=> array ('type' => 'integer', 'default' => 0),
				'active'		=> array ('type' => 'integer', 'default' => 1)
			);	

			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_articles";
			$database->build_and_execute( 
				'INSERT', 
				$table, 
				$all_values
			);			
		}
			
		// create sik directory for access files
		if(file_exists(LEPTON_PATH.'/'.$output_path.'/index.php') )
		{
			// rename dir to sik directory
			$new_dir_name = $output_path.'_sik';
			$current_dir_name = $output_path;
			rename(LEPTON_PATH.'/'.$current_dir_name, LEPTON_PATH.'/'.$new_dir_name);
		}

		//  Create articles post access files directory again and copy index.php to the output folder
		LEPTON_core::make_dir(LEPTON_PATH.'/'.$output_path);
		copy(LEPTON_PATH.'/temp/index.php',LEPTON_PATH.'/'.$output_path.'/index.php');
		
		// get all articles now from cleaned table
		$all_articles = [];
		$database->execute_query(
			"SELECT * FROM ".$table,
			true,
			$all_articles,
			true
		);

		LEPTON_handle::register("save_filename");
		// write new access file for each article
		foreach($all_articles as $new_file)
		{
			$post_id = $new_file['post_id'];
			$temp_name = save_filename($new_file['post_title']);

			$database->simple_query("UPDATE ".$table." SET `post_link` = '".$temp_name."' WHERE `post_id` = ".$new_file['post_id']);	
			$file_name = LEPTON_PATH.'/'.$output_path.'/'.$temp_name.'.php';
			
			// write access file for inserted article
			$content = ''.
	'<?php

	/**
	 *	This access file is autogenerated by addon Articles
	 *	custom license:https://cms-lab.com/_documentation/articles/license.php
	 *	Do not modify this file! 
	 */


	if(isset($_POST["show_detail"]))
	{
		$_POST["show_detail"] = '.$post_id.';
		$page_id = intval($_POST["page_id"] ?? 0);
	}

	require("../index.php");
	';
			
			$handle = fopen($file_name, 'w');
			fwrite($handle, $content);
			fclose($handle);
			LEPTON_core::change_mode($file_name);	
		}

		// delete sik directory
		LEPTON_handle::delete_obsolete_directories('/'.$output_path.'_sik');
	
		echo (LEPTON_tools::display('Table <b>mod_news_posts</b> finished','pre','ui positive message')); 			
	}
	else
	{
		echo (LEPTON_tools::display('There are no news in table <b>mod_news_posts</b>!','pre','ui positive message')); 	
	}
	
	// set settings table to news = imported
	$database->get_one("UPDATE ".TABLE_PREFIX."mod_articles_settings SET `import_news` = 1 WHERE `id` = 1 ");
	

	
	// import finished
	$message = "
	Import data from news addon finished.</br>
	Please check if old access files directory is still existing:</br>
	".LEPTON_PATH."/".$new_dir_name.",</br>
	If yes delete it manually, is not needed any more.</br>
	";	
	echo (LEPTON_tools::display($message,'pre','ui positive message')); 	
}	
else
{
	echo (LEPTON_tools::display('Upgrade Articles: <a href="'.LEPTON_URL.'/modules/articles/README.md" target="_blank">See readme file</a>, if you want to import data from news addon.','pre','ui positive message'));
}

