<?php

declare(strict_types=1);

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
 
class articles_simplepagehead
{
	use LEPTON_singleton;
	
	public string $keywords = 'LEPTON, CMS, Project';
	
  
    public static function get_addon_infos ( $iPageId = PAGE_ID ):array
    {	
		$entries = [
		    'keywords' => KEYWORDS,
		    'title'     => PAGE_TITLE,
		    'description' => DESCRIPTION,
			'tag' => ""		// use for rss and atom feed head link tag
		];
		
		if( isset($_POST["show_detail"]) && is_numeric($_POST["show_detail"]))
		{
			$post_id = LEPTON_core::getValue('show_detail');
			$database = LEPTON_database::getInstance();
			$database->execute_query(
				"SELECT post_title, post_teaser, post_tags FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$post_id,
				true,
				$entries,
				false
			);
			
			// load droplet engine and process for droplets can be part of content or teaser
			LEPTON_handle::include_files ('/modules/droplets/droplets.php');
			foreach($entries as &$temp_entry)
			{
				evalDroplets($temp_entry);				
			}
			
			// return htmlspecial chars to tags and remove tags 
			$entries['keywords'] = strip_tags(html_entity_decode($entries['post_tags'],ENT_HTML5));
			$entries['title'] = strip_tags(WEBSITE_TITLE.' - '.html_entity_decode($entries['post_title'],ENT_HTML5));
			$entries['description'] = strip_tags(html_entity_decode($entries['post_teaser'],ENT_HTML5));			
			
			// strip quotes and cut length
			$sEntryLength = 180;
			$sLength = $sEntryLength - 40;
			foreach ($entries as $key => &$temp_entry)
			{
				$sValue = str_replace('"', '', $temp_entry); 
				if (strlen($sValue) > $sEntryLength) 
				{
					if(preg_match('/.{0,'.$sEntryLength.'}(?:[.!?:,])/su', $sValue, $match)) 
					{
						$sValue = $match[0];
					}			
					if (strlen($sValue) > $sEntryLength) 
					{
						$pos = strpos($sValue, " ", $sLength);
						if ($pos > 0) 
						{
							$sValue = substr($sValue, 0,  $pos);
						}					
					}
				}
				
				$temp_entry = $sValue;
			}
		
			$entries['post_id'] = $post_id;
			
			return $entries;
		}
		else
		{
			return $entries;						
		}
    }	
}