<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
class articles_frontend extends LEPTON_abstract
{
	public array $all_articles = [];
	public array $all_user  = [];
	public string $fe_action = '';
	public bool $display_details = false;
	public string $output_path = '';
	public string $image_url ='';
	public string $image_path ='';
	public string $search_results = '';
		
	public object|null $oTwig = null;
	public LEPTON_database $database;
	public static $instance;
	
	public function initialize() 
	{		
		$this->database = LEPTON_database::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('articles');		

		$this->fe_action = LEPTON_URL.PAGES_DIRECTORY.LEPTON_frontend::getInstance()->page['link'].PAGE_EXTENSION;	
		$this->output_path = $this->database->get_one("SELECT dir_name FROM ".TABLE_PREFIX."mod_articles_settings WHERE id = 1 ");
		$this->image_url = LEPTON_URL.MEDIA_DIRECTORY.'/articles/';
		$this->image_path = LEPTON_PATH.MEDIA_DIRECTORY.'/articles';
		
		if(!defined('ARTICLES_SEARCH_URL'))
		{
			define('ARTICLES_SEARCH_URL', LEPTON_URL."/modules/".$this->output_path."/frontend_result.php");
		}
				
		// get data from table
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE active = 1 ORDER BY post_id DESC",
			true,
			$this->all_articles,
			true
		);

		//get all user
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."users " ,
			true,
			$this->all_user,
			true
		);	
		
		// look for outdated articles and set them active = 0
		$today = time();
		// based on publish_end
		foreach($this->all_articles as $temp )
		{
			if($temp['publish_end'] != NULL)
			{
				$end = strtotime($temp['publish_end']);	
				if($end < $today) 
				{
					$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles SET `active` = 0 WHERE post_id = ".$temp['post_id']);
				}
			}
		}
		
		// start search tags
		if(isset($_POST['search_tag']) && $_POST['search_tag'] != '')
		{
			$this->search_results = $this->search_tags($_POST['search_tag']);
	
		}			
	}
	
	public function articles_all($max = 100)
	{
		// create droplet output
		if(isset($_POST['show_detail'])) 			
		{
			return $this->show_detail(LEPTON_core::getValue('show_detail'));
		}		

		if($max == -1)
		{
			$query = "SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE active = 1 ORDER BY post_id DESC ";
		}
		else
		{
			$query = "SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE active = 1 ORDER BY post_id DESC LIMIT ".$max;
		}

		$selection = [];
		// get data from table
		$this->database->execute_query(
			$query,
			true,
			$selection,
			true
		);

		// get tags
		foreach($selection as &$check_tags)
		{		
			if($check_tags['post_tags'] != '')			
			{
				$check_tags['post_tags'] = explode(",",$check_tags['post_tags']);	
			}
			else
			{
				$check_tags['post_tags'] = [];
			}		
		}
		
		// data for twig template engine	
		$data = array(
			'oAFE'		=> $this,
			'page_id'	=> PAGE_ID ?? 0,
			'PAGE_TITLE'=> PAGE_TITLE,
			'selection'	=> $selection
		);

		$template = '@articles/frontend/list_all.lte';
		if(file_exists(LEPTON_PATH.'/modules/articles/templates/frontend_custom/list_all.lte'))
		{
			$template = '@articles/frontend_custom/list_all.lte';
		}
		
		//	get the template-engine	
		return $this->oTwig->render($template, $data);
	}
	

	public function articles_group($group_id = 1,$max = -1)
	{
		// create droplet output
		if(isset($_POST['show_detail'])) 			
		{
			return $this->show_detail(LEPTON_core::getValue('show_detail'));
		}		

		if($max == -1)
		{
			$query = "SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE active = 1 AND group_id = ".$group_id." ORDER BY post_id DESC ";
		}
		else
		{
			$query = "SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE active = 1 AND group_id = ".$group_id." ORDER BY post_id DESC LIMIT ".$max." ";
		}

		$selection = [];
		// get data from table
		$this->database->execute_query(
			$query,
			true,
			$selection,
			true
		);

		// data for twig template engine	
		$data = array(
			'oAFE'		=> $this,
			'page_id'	=> PAGE_ID ?? 0,
			'PAGE_TITLE'=> PAGE_TITLE,
			'selection'	=> $selection
		);

		$template = '@articles/frontend/list_all.lte';
		if(file_exists(LEPTON_PATH.'/modules/articles/templates/frontend_custom/list_all.lte'))
		{
			$template = '@articles/frontend_custom/list_all.lte';
		}
		
		//	get the template-engine	
		return $this->oTwig->render($template, $data);
	}


	public function show_detail($id = -1)
	{		
		$post_id = LEPTON_core::getValue('show_detail');
		
		$current_article = [];
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$post_id,
			true,
			$current_article,
			false
		);
		
		// get tags
		if($current_article['post_tags'] != '')
		{
			$tags = explode(",", $current_article['post_tags']);
		}
		else
		{
			$tags = [];
		}
 
		// data for twig template engine	
		$data = array(
			'oAFE'		=> $this,
			'back'		=> $_POST['back'],
			'page_id'	=> $_POST['page_id'],			
			'tags'		=> $tags,
			'article'	=> $current_article
		);
		
		$template = '@articles/frontend/detail.lte';
		if(file_exists(LEPTON_PATH.'/modules/articles/templates/frontend_custom/detail.lte'))
		{
			$template = '@articles/frontend_custom/detail.lte';
		}		

		//	get the template-engine	
		return $this->oTwig->render($template, $data);
	}
	
	public function search_tags($tag = '')
	{		
		$tag = LEPTON_core::getValue('search_tag');
		$searched_articles = [];
			
		foreach($this->all_articles as $search_articles)
		{
			if (str_contains($search_articles['post_tags'], $tag)) 
			{
				$this->database->execute_query(
					"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$search_articles['post_id'],
					true,
					$search_articles,
					false
				);
				$searched_articles[] = $search_articles;
			}			
		}

		// data for twig template engine	
		$data = array(
			'oAFE'		=> $this,
			'back'		=> $_POST['back'],
			'page_id'	=> $_POST['page_id'],			
			'tag'		=> $tag,
			'searched_articles'	=> $searched_articles
		);
	
		$template = '@articles/frontend/search_result.lte';
		if(file_exists(LEPTON_PATH.'/modules/articles/templates/frontend_custom/search_result.lte'))
		{
			$template = '@articles/frontend_custom/search_result.lte';
		}		

		//	get the template-engine	
		return $this->oTwig->render($template, $data);
	}		
}
