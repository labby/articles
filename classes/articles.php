<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
class articles extends LEPTON_abstract
{
	public array $all_articles = [];
	public array $active_articles = [];
	public array $all_user = [];
	public array $active_groups = [];
	public array $all_groups = [];
	public string $addon_color = 'blue';
	public string $action_url = ADMIN_URL . '/admintools/tool.php?tool=articles';
	public string $action = LEPTON_URL . '/modules/articles/';
	public string $output_path = '';
	public string $image_path = '';
		
	public object|null $oTwig = null;
	public LEPTON_database $database;
	public LEPTON_admin $admin;
	public static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->admin = LEPTON_admin::getInstance();		
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('articles');
		$this->image_path = LEPTON_PATH.MEDIA_DIRECTORY.'/articles';		
	}
	
	public function init_tool($iUserID = 0)
	{
		if($iUserID  == 0 || !is_integer($iUserID)) 
		{
			die('[0]');
		}
		
		// get output path from db
		$this->output_path = $this->database->get_one("SELECT dir_name FROM ".TABLE_PREFIX."mod_articles_settings WHERE id = 1 ");
		
		//get all articles
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_articles ORDER BY post_id DESC",
			true,
			$this->all_articles,
			true
		);		

		//get all groups
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_articles_groups " ,
			true,
			$this->all_groups,
			true
		);

		//get active groups
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_articles_groups WHERE active = 1" ,
			true,
			$this->active_groups,
			true
		);

		//get all user
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."users " ,
			true,
			$this->all_user,
			true
		);			
	}		


public function get_dataform( $iUserID = 0 )
	{
		if($iUserID  == 0 || !is_integer($iUserID)) 
		{
			die('[1]');
		}


		if(isset ($_POST['activate_article']) && ($_POST['activate_article'] != '') )
		{
			$id = LEPTON_core::getValue('activate_article');
			
			$status = $this->database->get_one("SELECT active FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$id);
			
			if($status == 0)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles SET `active` = 1 WHERE post_id = ".$id);
			}
			else
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles SET `active` = 0 WHERE post_id = ".$id);
			}
			
			$this->admin->print_success($this->language['save_ok'], $this->action_url);			
		}	
		

		if(isset ($_POST['duplicate_article']) && ($_POST['duplicate_article'] != '') )
		{
			$id = LEPTON_core::getValue('duplicate_article');
			$current_date = date("Y-m-d");

			$to_copy = [];
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$id ,
				true,
				$to_copy,
				false
			);

			LEPTON_handle::register("save_filename");
			
			$_POST['post_title'] = 'COPY-'.$to_copy['post_title'];
			$_POST['post_link'] = save_filename('COPY-'.$to_copy['post_link']);
			$_POST['post_url'] = 'COPY-'.$to_copy['post_url'];
			$_POST['post_teaser'] = $to_copy['post_teaser'];
			$_POST['post_content'] = $to_copy['post_content'];
			$_POST['post_tags'] = $to_copy['post_tags'];
			$_POST['group_id'] = $to_copy['group_id'];
			$_POST['publish_start'] = $current_date;
			$_POST['publish_end'] = $current_date;
			$_POST['posted_when'] = strtotime($current_date);
			$_POST['posted_by'] = $_SESSION['USER_ID'];
			$_POST['modified_when'] = strtotime($current_date);
			$_POST['modified_by'] = $_SESSION['USER_ID'];

			$request = LEPTON_request::getInstance();
		
			$all_names = array (
				'post_title'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_link'		=> array ('type' => 'string_clean', 'default' => ""),
				'post_url'		=> array ('type' => 'string_clean', 'default' => ""),				
				'post_teaser'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_content'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_tags'		=> array ('type' => 'string_clean', 'default' => ""),
				'group_id'		=> array ('type' => 'integer', 'default' => 0),
				'publish_start'	=> array ('type' => 'date', 'default' => ''),
				'publish_end'	=> array ('type' => 'date', 'default' => ''),
				'posted_when'	=> array ('type' => 'integer', 'default' => 0),
				'posted_by'		=> array ('type' => 'integer', 'default' => 0),
				'modified_when'	=> array ('type' => 'integer', 'default' => 0),
				'modified_by'	=> array ('type' => 'integer', 'default' => 0),
			);		

			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_articles";
			$this->database->build_and_execute( 
				'INSERT', 
				$table, 
				$all_values
			);

			// get inserted ID
			$last_id = $this->database->get_one("SELECT LAST_INSERT_ID() FROM ".$table);
			
			// write access file for inserted article
			$content = ''.
'<?php

/**
 *	This access file is autogenerated by addon Articles
 *	custom license:https://cms-lab.com/_documentation/articles/license.php
 *	Do not modify this file! 
 */


if(isset($_POST["show_detail"]))
{
	$_POST["show_detail"] = '.$last_id.';
	$page_id = intval($_POST["page_id"] ?? 0);
}

require("../index.php");
';
			$file_name = LEPTON_PATH.'/'.$this->output_path.'/'.$_POST['post_link'].'.php';
			$handle = fopen($file_name, 'w');
			fwrite($handle, $content);
			fclose($handle);
			LEPTON_core::change_mode($file_name);

			$this->admin->print_success($this->language['save_ok'], $this->action_url);				

		}


		if(isset ($_POST['edit_article']) && ($_POST['edit_article'] != '') ) 
		{
			LEPTON_handle::register("display_wysiwyg_editor");

			$id = LEPTON_core::getValue('edit_article');
			
			$current_article = [];
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE post_id = ".$id ,
				true,
				$current_article,
				false
			);
			
			$display_name = $this->database->get_one("SELECT display_name FROM ".TABLE_PREFIX."users WHERE user_id = ".$current_article['posted_by']);
			$modified_name = $this->database->get_one("SELECT display_name FROM ".TABLE_PREFIX."users WHERE user_id = ".$current_article['modified_by']);
			
			// data for twig template engine	
			$data = array(
				'oArt'				=> $this,
				'current_article'	=> $current_article,
				'display_name'		=> $display_name,
				'modified_name'		=> $modified_name,
				'display_content'  	=> display_wysiwyg_editor('post_content','content', $current_article['post_content'],'100','350', false),	
				'leptoken'			=> get_leptoken()
			);		

			//	get the template-engine
			$template = '@articles/backend/edit_article.lte';			
			echo $this->oTwig->render($template, $data);
		}
		

		if(isset ($_POST['save_article']) && ($_POST['save_article'] != '') ) 
		{
			$id = LEPTON_core::getValue('save_article');
			$table = TABLE_PREFIX."mod_articles";
			$current_date = date("Y-m-d");
			
			LEPTON_handle::register("save_filename");
			$_POST['post_link'] = save_filename($_POST['post_link']);

			// get old filename
			$old_filename = $this->database->get_one("SELECT post_link FROM ".$table." WHERE post_id = ".$id);
			if(!file_exists($old_filename))
			{
				// write acess file to get FE to work
				$content = ''.
'<?php

/**
 *	This access file is autogenerated by addon Articles
 *	custom license:https://cms-lab.com/_documentation/articles/license.php
 *	Do not modify this file! 
 */


if(isset($_POST["show_detail"]))
{
	$_POST["show_detail"] = '.$_POST['post_id'].';
	$page_id = intval($_POST["page_id"] ?? 0);
}

require("../index.php");
';
				$file_name = LEPTON_PATH.'/'.$this->output_path.'/'.$_POST['post_link'].'.php';
				$handle = fopen($file_name, 'w');
				fwrite($handle, $content);
				fclose($handle);
				LEPTON_core::change_mode($file_name);				
			}
			
			if($old_filename != $_POST['post_link'])
			{
				// copy access file with new name and delete old one
				rename(LEPTON_PATH.'/'.$this->output_path.'/'.$old_filename.'.php',LEPTON_PATH.'/'.$this->output_path.'/'.$_POST['post_link'].'.php');
			}
			
			$_POST['posted_when'] = strtotime($_POST['posted_when']);
			$_POST['modified_when'] = strtotime($current_date);
			$_POST['modified_by'] = $_SESSION['USER_ID'];

			$request = LEPTON_request::getInstance();
			$all_names = array (
				'post_title'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_link'		=> array ('type' => 'string_clean', 'default' => ""),
				'post_url'		=> array ('type' => 'string_clean', 'default' => ""),				
				'post_teaser'	=> array ('type' => 'string_clean', 'default' => ""),
				'post_content'	=> array ('type' => 'string_chars', 'default' => ""),
				'post_tags'		=> array ('type' => 'string_clean', 'default' => ""),
				'group_id'		=> array ('type' => 'integer', 'default' => 0),
				'publish_start'	=> array ('type' => 'date', 'default' => NULL),
				'publish_end'	=> array ('type' => 'date', 'default' => NULL),
				'posted_when'	=> array ('type' => 'integer', 'default' => 0),
				'posted_by'		=> array ('type' => 'integer', 'default' => 0),
				'modified_when'	=> array ('type' => 'integer', 'default' => 0),
				'modified_by'	=> array ('type' => 'integer', 'default' => 0),
			);		

			$all_values = $request->testPostValues($all_names);	

			$this->database->build_and_execute( 
				'UPDATE', 
				$table, 
				$all_values,
				'post_id = '.$id
			);

			$this->admin->print_success($this->language['save_ok'], $this->action_url);				
		}	


		if(isset ($_POST['delete_article']) && ($_POST['delete_article'] != '') ) 
		{
			$id = LEPTON_core::getValue('delete_article');
			$table = TABLE_PREFIX."mod_articles";

			// delete access file
			$link_url = $this->database->get_one("SELECT link_url FROM ".$table." WHERE post_id = ".$id);
			unlink(LEPTON_PATH.'/'.$this->output_path.'/'.$link_url.'.php');

			$this->database->simple_query("DELETE FROM ".$table." WHERE post_id = ".$id);	
			$this->admin->print_success($this->language['save_ok'], $this->action_url);	
		}


		if(isset ($_POST['list_groups']) && ($_POST['list_groups'] == 'list') ) 
		{
			// data for twig template engine	
			$data = array(
				'oArt'			=> $this,	
				'readme_link'	=> "https://forum.lepton-cms.org",			
				'leptoken'		=> get_leptoken()
			);

			//	get the template-engine
			$template = '@articles/backend/groups.lte';			
			echo $this->oTwig->render($template, $data);
		}


		if(isset ($_POST['edit_group']) && ($_POST['edit_group'] != '') ) 
		{
			$id = LEPTON_core::getValue('edit_group');
			
			$current_group = [];
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_articles_groups WHERE group_id = ".$id ,
				true,
				$current_group,
				false
			);
			
			// data for twig template engine	
			$data = array(
				'oArt'			=> $this,
				'current_group'	=> $current_group,
				'leptoken'		=> get_leptoken()
			);

			//	get the template-engine
			$template = '@articles/backend/edit_group.lte';			
			echo $this->oTwig->render($template, $data);
		}
		

		if(isset ($_POST['save_group']) && ($_POST['save_group'] != '') ) 
		{
			$id = LEPTON_core::getValue('save_group');
			$title = LEPTON_core::getValue('group_title');
			
			$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles_groups SET `group_title` = '".$title."' WHERE group_id = ".$id);
			$this->admin->print_success($this->language['save_ok'], $this->action_url);	
		}
		

		if(isset ($_POST['delete_group']) && ($_POST['delete_group'] != '') )
		{
			$id = LEPTON_core::getValue('delete_group');

			$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_articles_groups WHERE group_id = ".$id);	
			$this->admin->print_success($this->language['save_ok'], $this->action_url);	
		}	
		

		if(isset ($_POST['add_group']) && ($_POST['add_group'] == 'new') ) 
		{
			$field_values="(NULL,'group_title',1)";
			LEPTON_handle::insert_values("mod_articles_groups",$field_values);
			
			$this->admin->print_success($this->language['save_ok'], $this->action_url);	
		}	


		if(isset ($_POST['activate_group']) && ($_POST['activate_group'] != '') )
		{
			$id = LEPTON_core::getValue('activate_group');

			$status = $this->database->get_one("SELECT active FROM ".TABLE_PREFIX."mod_articles_groups WHERE group_id = ".$id);
			
			if($status == 0)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles_groups SET `active` = 1 WHERE group_id = ".$id);
				$this->admin->print_success($this->language['save_ok'], $this->action_url);
			}
			else
			{
				// check if group is in use
				$check_group = [];
				$this->database->execute_query(
					"SELECT * FROM ".TABLE_PREFIX."mod_articles WHERE group_id = ".$id ,
					true,
					$check_group,
					true
				);
				
				if(!empty($check_group))
				{
					$this->admin->print_error($this->language['group_in_use'], $this->action_url);	
				}
				else
				{
					$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles_groups SET `active` = 0 WHERE group_id = ".$id);
					$this->admin->print_success($this->language['save_ok'], $this->action_url);
				}	
			}		
		}	


		if(isset ($_POST['modify_settings']) && ($_POST['modify_settings'] == 1) )
		{
			// data for twig template engine	
			$data = array(
				'oArt'			=> $this,			
				'leptoken'		=> get_leptoken()
			);

			//	get the template-engine
			$template = '@articles/backend/settings.lte';			
			echo $this->oTwig->render($template, $data);
		}


		if(isset ($_POST['save_settings']) && ($_POST['save_settings'] == 1) ) 
		{
			$id = 1;
			
			if($_POST['current_dir_name'] != $_POST['dir_name'])
			{
				
				$new_dir_name = LEPTON_core::getValue('dir_name');
				$current_dir_name = $_POST['current_dir_name'];
		
				// rename dir to new directory
				rename(LEPTON_PATH.'/'.$current_dir_name, LEPTON_PATH.'/'.$new_dir_name);
				
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles_settings SET `dir_name` = '".$new_dir_name."' WHERE id = ".$id);
				$this->admin->print_success($this->language['save_ok'], $this->action_url);		
			}
			else
			{
				$this->admin->print_success($this->language['save_ok'], $this->action_url);		
			}

		}	
		
		if(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) 
		{
			// create links	
			$support_link = "<a href='#'>NO Live-Support / FAQ</a>";
			$readme_link = "<a href='https://forum.lepton-cms.org' class='info' target='_blank'>Forum</a>";		

			// data for twig template engine	
			$data = array(
				'oArt'			=> $this,
				'readme_link'	=> $readme_link,		
				'SUPPORT'		=> $support_link,		
				'image_url'		=> $this->action.'img/articles.png'
			);

			//	get the template-engine
			$template = '@articles/backend/info.lte';			
			echo $this->oTwig->render($template, $data);

		}

		if(isset ($_GET['tool']) && (empty($_POST)) || isset($_POST['list_articles']) )
		{			
			// data for twig template engine	
			$data = array(
				'oArt'			=> $this,	
				'readme_link'	=> "https://forum.lepton-cms.org",			
				'leptoken'		=> get_leptoken()
			);
	
			//	get the template-engine
			$template = '@articles/backend/articles.lte';			
			echo $this->oTwig->render($template, $data);
		}
	}
}
