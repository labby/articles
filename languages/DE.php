<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$MOD_ARTICLES = [
	'action'	    => "Aktion",
	'add'	    	=> "Eintrag hinzufügen",
	'all_articles'	=> "Alle Artikel",
	'all_groups'  	=> "Alle Gruppen",
	'article'	    => "Artikel",
	'author'	    => "Verfasser",
	'content'	    => "Inhalt",
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'details'	    => "Details",
	'dir_name'		=> "Ausgabepfad Frontend",
	'duplicate'     => "Kopieren",
	'edit'	        => "Bearbeiten",	
	'error'	        => "FEHLER",
	'external_link' => "Weiterführende Informationen: ",
	'info'	        => "Addon Info",
	'group'	    	=> "Gruppe",
	'group_in_use'	=> "Gruppe kann nicht deaktiviert werden, da sie noch benutzt wird!",
	'header1'	    => "ID",
	'help'	    	=> "Hilfe-Seite",
	'list_frontend_groups' => "Artikel Ausgabe je Gruppe",
	'modified_by'	=> "Geändert von",
	'modified_when'	=> "Geändert am",	
	'modify_path'	=> "Ausgabepfad editieren",	
	'modify_settings'	=> "Einstellungen editieren",
	'no_entry'	    => "Kein Ergebnis",
	'notice'	    => "Bemerkung",
	'posted_by'		=> "Erstellt von",
	'posted_when'	=> "Erstellt am",	
	'post_link' 	=> "Interner Link",
	'post_tags' 	=> "Tags",
	'post_title' 	=> "Artikel Titel",
	'post_url' 		=> "Externe URL",
	'published' 	=> "Veröffentlicht",	
	'publish_start' => "Start Veröffentlichtung",
	'publish_end' 	=> "Ende Veröffentlichung",
	'read_more'	    => "Weiter lesen",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'save_settings' => "Einstellungen speichern",
	'search_result' => "Ergebnis für Suche nach #",
	'select'	    => "Bitte auswählen",
	'tags'	    	=> "Tags",
	'teaser'	    => "Teaser",
	'teaser_image'  => "Individuelles Bild für den Teaser gewünscht?",
	'teaser_image_text1'  => "Dann Bild (.png) in der entsprechenden Größe hinterlegen im Verzeichnis",
	'teaser_image_text2'  => "und als Namen die Post-ID verwenden:",
	'to_delete'	    => "wirklich löschen",
	'want_delete'	=> "Wollen Sie den Datensatz"
];
