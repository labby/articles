<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$MOD_ARTICLES = [
	'action'	    => "Action",
	'add'	    	=> "Add entry",
	'all_articles'	=> "All Articles",
	'all_groups'  	=> "All Groups",
	'article'	    => "Article",
	'author'	    => "Author",
	'content'	    => "Content",
	'delete_ok'     => "Data deleted succesfully",
	'details'	    => "Details",
	'dir_name'		=> "Output-Url Frontend",
	'duplicate'     => "copy",
	'edit'	        => "edit",	
	'error'	        => "ERROR",
	'external_link' => "More Informations: ",
	'info'	        => "Addon Info",
	'group'	    	=> "Group",
	'group_in_use'	=> "Group in use, deactivation impossible!",
	'header1'	    => "ID",
	'help'	    	=> "Help",
	'list_frontend_groups'=> "Articles listed by group",
	'modified_by'	=> "Modified by",
	'modified_when'	=> "Modified ",	
	'modify_path'	=> "Edit ourput-url",	
	'modify_settings'	=> "Edit settings",
	'no_entry'	    => "No result",
	'notice'	    => "Notice",
	'posted_by'		=> "Posted by",
	'posted_when'	=> "Posted",	
	'post_link' 	=> "Internal Link",
	'post_tags' 	=> "Tags",
	'post_title' 	=> "Article Title",
	'post_url' 		=> "External URL",
	'published' 	=> "Published",	
	'publish_start' => "Start publish",
	'publish_end' 	=> "End publish",
	'read_more'	    => "Read more",
	'save_ok'	    => "Data saved successfully",
	'save_settings' => "Save settings",
	'search_result' => "Search-Result #",
	'select'	    => "Please select",
	'tags'	    	=> "Tags",
	'teaser'	    => "Teaser",
	'teaser_image'  => "Individual image wanted for Teaser?",
	'teaser_image_text1'  => "Save image (.png) in accordingly size into directory",
	'teaser_image_text2'  => "and use Post-ID as name:",
	'to_delete'	    => "really",
	'want_delete'	=> "Do you want to delete"
];
