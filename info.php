<?php

/**
 * @module          Articles
 * @author          cms-lab
 * @copyright       2024-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory	= 'articles';
$module_name		= 'Articles';
$module_function	= 'tool';
$module_version		= '1.1.0';
$module_platform	= '7.x';
$module_delete		=  true;
$module_author		= 'cms-lab';
$module_home		= 'https://cms-lab.com';
$module_guid		= '227c75f5-3679-4aac-8bd6-8d37825529b6';
$module_license		= '<a href="https://cms-lab.com/_documentation/articles/license.php" target="_blank">Custom License</a>';
$module_license_terms	= '<a href="https://cms-lab.com/_documentation/articles/license.php" target="_blank">License Terms</a>';
$module_description	= 'Admintool to handle articles, news, items etc.';
