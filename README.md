### Articles
=========

LEPTON Admintool to handle articles, news, items etc. <br />
Output via templates and Droplets (output anywhere) with individuell output directory, article title, description, keywords and tags.


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
 

#### Installation

* download latest [Articles.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon go to admintools, enter Articles and start to work. <br />
For further details please see [forum][3].<br />
<br />
Please keep in mind to import the tool frontend.css into our frontend template!<br />
Example: @import "../../../modules/articles/css/frontend.css"; <br />
<br />


#### Allow import data from existing news addon

If you want to import news data from your table copy following code in a code2 page and run it from the frontend:<br />
`$database->simple_query("UPDATE ".TABLE_PREFIX."mod_articles_settings SET `import_news` = 0 WHERE `id` = 1 ");`
<br />


#### Import data from existing news addon

* CAUTION: all existing data in articles tables are deleted, but they are automatically backed up in xsik_tables in your database.
* USAGE: run the upgrade file manually from add-ons/modules in the backend.<br />
* SUPPORT: if you ran into problems or for further questions please use the [forum][3]<br />




[1]: https://lepton-cms.org
[2]: http://www.lepton-cms.com/lepador/admintools/articles.php
[3]: https://forum.lepton-cms
